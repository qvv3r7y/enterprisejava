package com.qvv3r7y;

import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {
    private static final Logger logger = LogManager.getLogger(App.class);

    public static void main(String[] args) {
        Options options = initOptions();
        CommandLine commandLine;
        try {
            commandLine = new DefaultParser().parse(options, args);
        } catch (ParseException e) {
            logger.error(e.getMessage());
            new HelpFormatter().printHelp("STATISTIC PARSER", options);
            return;
        }
        String path = commandLine.getOptionValue("input");

        OSMParser parser = new OSMParser(path);
        parser.parse();
        printStatistic(parser);
    }

    public static void printStatistic(OSMParser parser) {
        logger.info("Print statistic");
        var sortedUserActivity = parser.getSortedUserActivity();
        var tagNameStatistic = parser.getTagNameStatistic();

        System.out.println("\nUser Activity :::\n");
        for (var stat : sortedUserActivity) {
            System.out.println(stat);
        }

        System.out.println("\nTags name usage :::\n");
        for (var tags : tagNameStatistic.entrySet()) {
            System.out.println(tags);
        }
    }

    private static Options initOptions() {
        Options options = new Options();

        Option input = new Option("i", "input", true, "path for OSM data file");
        input.setRequired(true);
        options.addOption(input);

        return options;
    }


}
