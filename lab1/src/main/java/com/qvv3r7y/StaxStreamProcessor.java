package com.qvv3r7y;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;

public class StaxStreamProcessor implements AutoCloseable {
    private static final Logger logger = LogManager.getLogger(StaxStreamProcessor.class);
    private static final XMLInputFactory FACTORY = XMLInputFactory.newInstance();

    private final XMLStreamReader xmlStreamReader;

    public StaxStreamProcessor(InputStream inputStream) throws XMLStreamException {
        xmlStreamReader = FACTORY.createXMLStreamReader(inputStream);
    }

    public boolean doUntil(int targetEvent, String[] values) throws XMLStreamException {
        while (xmlStreamReader.hasNext()) {
            int event = xmlStreamReader.next();
            for (String value : values) {
                if (event == targetEvent && value.equals(xmlStreamReader.getLocalName())) {
                    return true;
                }
            }

        }
        return false;
    }

    public String getAttribute(String name) {
        return xmlStreamReader.getAttributeValue(null, name);
    }

    public String getText() throws XMLStreamException {
        return xmlStreamReader.getElementText();
    }

    public XMLStreamReader getXmlStreamReader() {
        return xmlStreamReader;
    }

    @Override
    public void close() {
        if (xmlStreamReader != null) {
            try {
                xmlStreamReader.close();
            } catch (XMLStreamException e) {
                e.printStackTrace();
                logger.error(e.getMessage());
            }
        }
    }
}
