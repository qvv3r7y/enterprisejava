package com.qvv3r7y;

import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLStreamConstants;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;

public class OSMParser {
    private static final Logger logger = LogManager.getLogger(OSMParser.class);
    private static final int BUFFER_SIZE = 4098;
    private final CompressorInputStream is;
    private final Map<String, Set<Long>> activityStatistic = new HashMap<>();
    private final Map<String, Long> tagNameStatistic = new HashMap<>();

    private SortedSet<Map.Entry<String, Integer>> sortedUserActivity;

    public OSMParser(String path) {
        try {
            FileInputStream fin = new FileInputStream(path);
            BufferedInputStream bis = new BufferedInputStream(fin, BUFFER_SIZE);
            is = new CompressorStreamFactory().createCompressorInputStream(bis);
        } catch (CompressorException | FileNotFoundException e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public void parse() {
        try (StaxStreamProcessor processor = new StaxStreamProcessor(is)) {
            logger.info("Unpack archive");

            while (processor.getXmlStreamReader().hasNext()) {
                if (!processor.doUntil(XMLStreamConstants.START_ELEMENT, new String[]{"node", "tag"})) continue;

                if ("tag".equals(processor.getXmlStreamReader().getLocalName())) {
                    String tagName = processor.getAttribute("k");

                    Long countNodes = tagNameStatistic.getOrDefault(tagName, 0L);
                    tagNameStatistic.put(tagName, ++countNodes);

                } else if ("node".equals(processor.getXmlStreamReader().getLocalName())) {
                    String userName = processor.getAttribute("user");
                    String changeset = processor.getAttribute("changeset");

                    activityStatistic.computeIfAbsent(userName, u -> new HashSet<>());
                    activityStatistic.get(userName).add(Long.parseLong(changeset));
                }

            }

            Map<String, Integer> userActivityStatistic = new TreeMap<>();
            for (Map.Entry<String, Set<Long>> userData : activityStatistic.entrySet()) {
                userActivityStatistic.put(userData.getKey(), userData.getValue().size());
            }
            sortedUserActivity = entriesSortedByValues(userActivityStatistic);

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public <K, V extends Comparable<? super V>> SortedSet<Map.Entry<K, V>> entriesSortedByValues(Map<K, V> map) {
        SortedSet<Map.Entry<K, V>> sortedEntries = new TreeSet<>((e1, e2) -> e2.getValue().compareTo(e1.getValue()));
        sortedEntries.addAll(map.entrySet());
        return sortedEntries;
    }

    public Map<String, Set<Long>> getActivityStatistic() {
        return activityStatistic;
    }

    public Map<String, Long> getTagNameStatistic() {
        return tagNameStatistic;
    }

    public SortedSet<Map.Entry<String, Integer>> getSortedUserActivity() {
        return sortedUserActivity;
    }
}
